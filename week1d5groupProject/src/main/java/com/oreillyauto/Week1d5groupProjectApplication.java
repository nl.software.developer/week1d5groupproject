package com.oreillyauto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Week1d5groupProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(Week1d5groupProjectApplication.class, args);
	}

}
